//
//  Film.swift
//  WorldCimema1
//
//  Created by Преподаватель on 10.11.2021.
//

import Foundation

class Film{
        
        enum filmsCategory: String{
            case new = "new";
            case inTrend = "inTrend";
            case forYou = "forYou"
        }
        
        func getAllFilms(filter: filmsCategory = .inTrend) -> [FilmModel]? {
            var films: [FilmModel]?
            
            let semaphore = DispatchSemaphore (value: 0)
            
            var request = URLRequest(url: URL(string: "http://cinema.areas.su/movies?filter=\(filter)")!,timeoutInterval: Double.infinity)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else {
                    semaphore.signal()
                    return
                }
                films = try? JSONDecoder().decode([FilmModel].self, from: data)
                semaphore.signal()
            }
            
            task.resume()
            semaphore.wait()
            
            return films
        }
        

}

