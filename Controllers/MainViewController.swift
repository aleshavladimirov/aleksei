//
//  MainViewController.swift
//  WorldCimema1
//
//  Created by Преподаватель on 10.11.2021.
//
// fvfgfhgvbcdfdbegergegerefddsdw
import UIKit

class MainViewController: UIViewController, UICollectionViewDataSource {
    @IBOutlet weak var filmCollectionView: UICollectionView!
    @IBOutlet weak var trendView: UIView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var forYouView: UIView!
    @IBOutlet weak var yourFilmsImage: UIImageView!
    
    
    var film = Film()
    var films = Film().getAllFilms()
    let cover = Cover().getCover()
    var color = UIColor()
    var buttonTab: Int = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        color = trendView.backgroundColor ?? .clear
        yourFilmsImage.image = UIImage.getImageFromUrl(url:"magicians.png")

    }
    
    
    @IBAction func trendButton(_ sender: Any) {
        setTab(0)
        
        films = film.getAllFilms(filter: .inTrend)
        filmCollectionView.reloadData()
    }
    
    @IBAction func newButton(_ sender: Any) {
        setTab(1)
        films = film.getAllFilms(filter: .new)
        filmCollectionView.reloadData()
    }
    
    @IBAction func forYouButton(_ sender: Any) {
        setTab(2)
        films = film.getAllFilms(filter: .forYou)
        filmCollectionView.reloadData()
    }
    func setTab(_ position: Int){
        if position == 0{
            
            trendView.backgroundColor = color
            newView.backgroundColor = .clear
            forYouView.backgroundColor = .clear
        }else if position == 1{
            
            trendView.backgroundColor = .clear
            newView.backgroundColor = color
            forYouView.backgroundColor = .clear
        }else if position == 2{
            
            trendView.backgroundColor = .clear
            newView.backgroundColor = .clear
            forYouView.backgroundColor = color
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        films?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let posterCell = collectionView.dequeueReusableCell(withReuseIdentifier: "filmsCell", for: indexPath) as! FilmsCollectionViewCell
        posterCell.posterImage.image = UIImage.getImageFromUrl(url: films![indexPath.item].poster)
        
        return posterCell
    }
    
}

