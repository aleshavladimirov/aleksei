//
//  SignInScreen.swift
//  WorldCimema1
//
//  Created by Преподаватель on 08.11.2021.
//

import UIKit

class SignInScreen: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var lastnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var returnPasswordField: UITextField!
    @IBOutlet weak var returnSignUpScreen: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CornerFunc2()
    }
    
 
    @IBAction func registryCheck(_ sender: Any) {
        
    if(!emailField.hasText || emailField.text == "E-mail") {
        let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный E-mail", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        return
    }
        if(!nameField.hasText || nameField.text == "Имя") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введено некорректное Имя", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if(!emailField.hasText || emailField.text == "Фамилия") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введена некорректная Фамилия", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if(!passwordField.hasText || passwordField.text == "Пароль") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный Пароль", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if passwordField.text != returnPasswordField.text{
            let alert = UIAlertController(title: "Ошибка ввода", message: "Пароли не совпадают", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if(!returnPasswordField.hasText || returnPasswordField.text == "Повторите пароль") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный пароль", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let email = emailField.text!
        let emailStr = email.split(separator: "@")
        if emailStr.count != 2 {
            let alert = UIAlertController(title: "Ошибка", message: "Нерпавильный формат E-mail" , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let emailStr1 = emailStr[1].split(separator: ".")
        if emailStr1.count < 2 || emailStr1.last!.count > 3 {
            let alert = UIAlertController(title: "Ошибка", message: "Неправильный формат E-mail", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert,animated: true, completion: nil)
            return
        }
        let emailStr2 = String(email.split(separator: "@").last?.split(separator: ".").last ?? "")
       
        let lowCaseStr1 = "qwertyuiopasdfghjklzxcvbnm"
        for item in emailStr2{
            if lowCaseStr1.contains(item){}else{
                let alert = UIAlertController(title: "Ошибка", message: "Неправильный формат E-mail", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
        }
        guard Registrate().register(email: emailField.text! , password: passwordField.text!, firstName: nameField.text!, lastName: lastnameField.text!) ?? false else{
            let alert = UIAlertController(title: "Ошибка", message: "Произошла ошибка при регистрации", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
            let vc = storyboard?.instantiateViewController(withIdentifier: "main")
            present (vc!, animated: true, completion: nil)
        }
    
    func CornerFunc2() {
        nameField.clipsToBounds = true
        nameField.layer.cornerRadius = 5
        nameField.layer.borderWidth = 1
        nameField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        lastnameField.clipsToBounds = true
        lastnameField.layer.cornerRadius = 5
        lastnameField.layer.borderWidth = 1
        lastnameField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        emailField.clipsToBounds = true
        emailField.layer.cornerRadius = 5
        emailField.layer.borderWidth = 1
        emailField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        passwordField.clipsToBounds = true
        passwordField.layer.cornerRadius = 5
        passwordField.layer.borderWidth = 1
        passwordField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        returnPasswordField.clipsToBounds = true
        returnPasswordField.layer.cornerRadius = 5
        returnPasswordField.layer.borderWidth = 1
        returnPasswordField.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        registrationButton.clipsToBounds = true
        registrationButton.layer.cornerRadius = 5
        registrationButton.layer.borderWidth = 1
        registrationButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        returnSignUpScreen.clipsToBounds = true
        returnSignUpScreen.layer.cornerRadius = 5
        returnSignUpScreen.layer.borderWidth = 1
        returnSignUpScreen.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
    }
    

}

