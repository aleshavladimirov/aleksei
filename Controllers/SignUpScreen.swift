//
//  SignUpScreen.swift
//  WorldCimema1
//
//  Created by Преподаватель on 08.11.2021.
//

import UIKit

class SignUpScreen: UIViewController {
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CornerFunc()
            }
    
    
    @IBAction func registryCheck(_ sender: Any) {
    
    
    
    if(!email.hasText || email.text == "E-mail") {
        let alert = UIAlertController(title: "Ошибка ввода", message: "Введите email", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        return
    }
        if(!password.hasText || password.text == "Пароль") {
            let alert = UIAlertController(title: "Ошибка ввода", message: "Введен некорректный Пароль", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        let email1 = email.text!
        let emailStr = email1.split(separator: "@")
        
        if emailStr.count != 2{
            let alert = UIAlertController(title: "Ошибка", message: "Нерпавильный формат E-mail1" , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let emailStr1 = emailStr[1].split(separator: ".")
        if emailStr1.count < 2 || emailStr1.last!.count > 3 {
            let alert = UIAlertController(title: "Ошибка", message: "Неправильный формат E-mail 2", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert,animated: true, completion: nil)
        }
        let emailStr2 = String(email1.split(separator: "@").last?.split(separator: ".").last ?? "")
        let lowCaseStr = "qwertyuiopasdfghjklzxcvbnm1234567890@."
        for item in emailStr2{
            if lowCaseStr.contains(item){}else{
                let alert = UIAlertController(title: "Ошибка", message: "Неправильный формат E-mail 3", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
                return
            }
        }
        let authorization = Authorization().login(email: email.text!, password: password.text!)
        guard authorization == "" else {
            let alert = UIAlertController(title: "Ошибка", message: "Неправильный Email или пароль", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "main")
        present (vc!, animated: true, completion: nil)
    
    }
    
    func CornerFunc (){
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.borderWidth = 1
        registrationButton.layer.cornerRadius = 5
        registrationButton.layer.borderWidth = 1
        registrationButton.clipsToBounds = true
        registrationButton.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        email.layer.cornerRadius = 5
        email.layer.borderWidth = 1
        email.clipsToBounds = true
        email.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor
        password.layer.cornerRadius = 5
        password.layer.borderWidth = 1
        password.clipsToBounds = true
        password.layer.borderColor = (UIColor(red:168, green: 168, blue: 168, alpha: 100)).cgColor

        
    }
}
   

