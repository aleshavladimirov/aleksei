//
//  CoverModel.swift
//  WorldCimema1
//
//  Created by Преподаватель on 10.11.2021.
//

import Foundation

struct CoverModel: Decodable{
    let movieId: Int
    let backgroundImage: String
    let foregroundImage: String
}
